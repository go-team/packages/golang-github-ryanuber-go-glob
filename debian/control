Source: golang-github-ryanuber-go-glob
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Dawid Dziurla <dawidd0811@gmail.com>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 12),
 dh-golang,
Build-Depends-Indep:
 golang-any,
Standards-Version: 4.5.0
Homepage: https://github.com/ryanuber/go-glob
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-ryanuber-go-glob
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-ryanuber-go-glob.git
XS-Go-Import-Path: github.com/ryanuber/go-glob
Testsuite: autopkgtest-pkg-go

Package: golang-github-ryanuber-go-glob-dev
Architecture: all
Depends:
 ${misc:Depends},
Multi-Arch: foreign
Description: Basic string globs in golang
 Go-glob is a single-function
 library implementing basic string glob support.
 .
 Globs are an extremely user-friendly way of supporting string matching
 without requiring knowledge of regular expressions or Go's particular
 regex engine. Most people understand that if you put a * character
 somewhere in a string, it is treated as a wildcard. Surprisingly,
 this functionality isn't found in Go's standard library, except for
 path.Match, which is intended to be used while comparing paths (not
 arbitrary strings), and contains specialized logic for this use case.
 A better solution might be a POSIX basic (non-ERE) regular expression
 engine for Go, which doesn't exist currently.
